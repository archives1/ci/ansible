# Ansible

## Install Dependentcy

*****This step support Ubuntu only**

Things to prepare before testing

| Tools | Version | Description |
| --- | --- | --- |
| [python3](#install-python-3-and-pip-3) | [3.10.4](https://www.python.org/downloads/release/python-3104/) | `sudo apt install -y python3` |
| [pip3](#install-python-3-and-pip-3) | [3.10.4](https://www.python.org/downloads/release/python-3104/) | `sudo apt install -y python3-pip` |
| [ssh server](#install-open-ssh-server) | 3.0.2 | `sudo apt install -y openssh-server` |
| [ansible](#installing-ansible-on-ubuntu) | [2.12.7](https://github.com/ansible/ansible/releases/tag/v2.12.7) | `sudo apt install -y ansible` |

#### Install Python 3 and pip 3

```sh

sudo apt update
sudo apt install -y software-properties-common
sudo add-apt-repository --yes --update ppa:deadsnakes/ppa
sudo apt install -y python3
sudo apt install -y python3-pip

```

#### Install Supporting Software

```sh

sudo apt install -y build-essential \
    zlib1g-dev \
    libncurses5-dev \
    libgdbm-dev \
    libnss3-dev \
    libssl-dev \
    libreadline-dev \
    libffi-dev wget

```

#### Test python3 and pip3

```sh

python3 -V && pip3 -V

```

Output:

```sh
Python 3.10.4
pip 22.0.2 from /usr/lib/python3/dist-packages/pip (python 3.10)
```


#### Installing Ansible on Ubuntu

[Installation Guide](https://docs.ansible.com/ansible/latest/installation_guide/installation_distros.html#installing-ansible-on-ubuntu)

```sh

sudo apt update
sudo apt install -y software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install -y ansible

```

#### Test ansible command

```sh

ansible --version

```

Output:

```sh
ansible [core 2.12.7]
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/sun/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/sun/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible
  python version = 3.10.4 (main, Apr  2 2022, 09:04:19) [GCC 11.2.0]
  jinja version = 3.0.3
  libyaml = True
```

#### Test ansible-playbook command

```sh

ansible-playbook --version

```

Output:

```sh
ansible-playbook [core 2.12.7]
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/home/sun/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python3/dist-packages/ansible
  ansible collection location = /home/sun/.ansible/collections:/usr/share/ansible/collections
  executable location = /usr/bin/ansible-playbook
  python version = 3.10.4 (main, Apr  2 2022, 09:04:19) [GCC 11.2.0]
  jinja version = 3.0.3
  libyaml = True
```

#### Install Open ssh server

[How to use ssh-keygen to generate a new SSH key](https://www.ssh.com/academy/ssh/keygen)

```sh
sudo apt install -y openssh-server
```

#### Creating an SSH Key Pair for User Authentication

(Upgrade Your SSH Key to Ed25519 on gcloud)[https://medium.com/risan/upgrade-your-ssh-key-to-ed25519-c6e8d60d3c54]

```sh
ssh-keygen
# OR
ssh-keygen -t rsa -b 4096
# OR
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519 -C "xxx@gm.com"
```

#### Get local ip address

```sh
ip a
```

#### Test ssh

```sh
ssh sun@192.168.1.111
```

#### Get Public Key

```sh
cat ~/.ssh/id_rsa.pub
```

#### Copying the Public Key to the Server

```sh
ssh-copy-id -i ~/.ssh/id_rsa.pub sun@192.168.1.111
```

#### Look Authorized keys

```sh
cat ~/.ssh/authorized_keys
```

#### Seting ssh agent

```sh
eval $(ssh-agent) && ssh-add
echo "# ssh agent" >> ~/.bashrc
echo "alias ssha='eval \$(ssh-agent) && ssh-add'" >> ~/.bashrc
alias ssha
```

#### Set Environment

```sh
source ~/.bashrc
```

#### Create inventory file

```sh
echo "192.168.1.8" >> inventory/targets 
```

#### Running ad-hoc Commands

```sh
ansible all --key-file ~/.ssh/id_rsa -i inventory/targets -m ping
```

#### Create ansible.cfg

```
[defaults]
inventory = inventory/targets
private_key_file = ~/.ssh/id_rsa
```

#### Running all hosts

```sh
ansible all -m ping
```

#### Get see hosts

```sh
ansible all --list-hosts
```

#### Run ansible playbook example task01

```sh
ansible-playbook tasks/task01.yml
```


create hive table with multi character delimiter












