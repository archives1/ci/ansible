winrm quickconfig
winrm get winrm/config/Service
winrm get winrm/config/Winrs
winrm enumerate winrm/config/Listener


[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
$url = "https://raw.githubusercontent.com/ansible/ansible/devel/examples/scripts/ConfigureRemotingForAnsible.ps1"
$file = "$env:temp\ConfigureRemotingForAnsible.ps1"
(New-Object -TypeName System.Net.WebClient).DownloadFile($url, $file)
powershell.exe -ExecutionPolicy ByPass -File $file

winrm get winrm/config/Service
winrm get winrm/config/Winrs
winrm enumerate winrm/config/Listener

